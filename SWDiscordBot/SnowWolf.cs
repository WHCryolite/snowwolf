﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace SWDiscordBot
{
    class SnowWolf
    {
        private static List<string> logList = new List<string>();

        static void Main(string[] args) => new SnowWolf().RunBot().GetAwaiter().GetResult();

        private async Task RunBot()
        {
            var config = new DiscordSocketConfig
            {
                MessageCacheSize = 100
            };

            var client = new DiscordSocketClient(config);

            client.Log += Log;
            client.MessageReceived += MessageReceived;

            string discordToken = "dummy auth code";

            try
            {
                await client.LoginAsync(TokenType.Bot, discordToken);
                await client.StartAsync();
            }
            catch (Exception errMsg)
            {
                Console.WriteLine($"[EE] {errMsg}");
            }

            client.MessageUpdated += MessageUpdated;
            client.Ready += () =>
            {
                logList.Add("Bot connected!");
                Console.WriteLine("Bot Connected1");
                return Task.CompletedTask;
            };

            await Task.Delay(-1);
        }

        private Task Log(LogMessage msg)
        {
            logList.Add(msg.ToString());
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task MessageUpdated(Cacheable<IMessage, ulong> before, SocketMessage after, ISocketMessageChannel channel)
        {
            var message = await before.GetOrDownloadAsync();
            logList.Add($"[Message] <B>: {before} | <A>: {after}");
            Console.WriteLine($"[Message] <B>: {before} | <A>: {after}");
        }

        private async Task MessageReceived(SocketMessage message)
        {
            if (message.Content == "!test")
                await message.Channel.SendMessageAsync("Test OK. Bot functional.");

            if (message.Content == "!logs")
            {
                foreach (var item in logList)
                {
                    await message.Channel.SendMessageAsync(Format.Italics(item));
                }
            }
        }
    }
}

